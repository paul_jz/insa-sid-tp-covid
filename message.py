#!/usr/bin/env python3
import json
#import time
#replaced by the following two lines for testing purposes
import falsifytime
time = falsifytime.falsifytime()

#for generation of messages
from random import random
from hashlib import sha256

class Message:
	#the constructor can take various parameters
	#default is parameterless, it generates a message to be said
	#if type is not specified, it is to be imported from an export string or parsed export string
	#otherwise, if date is False the time is system time
		#else it is a float, then the date should be this parameter
	def __init__(self, msg="", msg_type=False, msg_date=False):

	#a method to compute the age of a message in days or in seconds
	def age(self, days=True):

	#a class method that generates a random message
	@classmethod
	def generate(self):


	#a method to convert the object to string data
	def __str__(self):

	#export/import
	def m_export(self):

	def m_import(self, msg):

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	myMessage = Message()
	time.sleep(1)
	mySecondMessage = Message(Message.generate(), Message.MSG_THEY)
	copyOfM = Message(myMessage.m_export())
	print(myMessage)
	print(mySecondMessage)
	print(copyOfM)
	time.sleep(0.5)
	print(copyOfM.age(True,True))
	time.sleep(0.5)
	print(copyOfM.age(False,True))
