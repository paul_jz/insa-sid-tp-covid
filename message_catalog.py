#!/usr/bin/env python3
from message import *

class MessageCatalog:
	#constructor, loads the messages (if there are any)
	def __init__(self, filepath):

	#destructor (optional, if you opened a file, close it)
	def __del__(self):

	#Import object content (can import batch of THEY SAID or other for testing purposes)
	def c_import(self, content):

	#export the messages of a given type from a catalog
	def c_export_type(self, msg_type):

	#Export the whole catalog under a text format (optional)
	def c_export(self):

	#add a Message object to the catalog
	def add_message(self, m, save=True):

	#remove all messages of msg_type that are older than max_age days
	#msg_type is false for deleting all messages of all types older than max_age
	def purge(self, max_age=14, msg_type=False):

	#Check if message string is in a category
	def check_msg(self, msg_str, msg_type=Message.MSG_THEY):

	#Say if I should quarantine based on the state of my catalog
	def quarantine(self, nb_heard=3):

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	catalog = MessageCatalog("test.json")
	catalog.add_message(Message())
	catalog.add_message(Message(Message.generate(), Message.MSG_THEY))
	print(catalog.c_export())
	time.sleep(0.5)
	catalog.add_message(Message(Message.generate(),Message.MSG_IHEARD))
	catalog.add_message(Message())
	print(catalog.c_export())
	time.sleep(0.5)
	catalog.add_message(Message())
	print(catalog.c_export())
	time.sleep(2)
	catalog.purge(2)
	print(catalog.c_export())
