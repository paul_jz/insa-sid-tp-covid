# insa-SID-TP-covid

## Objectifs
Un projet pour voir :
* une architecture client serveur (un peu distribuée)
* une API très simple

### Quêtes annexes
* travailler à plusieurs avec git
* quelques notions de choix stratégiques de stop covid…

## Bibliographie
* https://risques-tracage.fr/docs/risques-tracage.pdf
* https://github.com/DP-3T/documents/blob/master/public_engagement/cartoon/fr/combined_panels.pdf

## Librairies utilisées
* requests : https://riptutorial.com/fr/python-requests
* flask : https://flask.palletsprojects.com/en/1.1.x/installation/
* venv : https://virtualenv.pypa.io/en/latest/installation.html   
([installer flask dans l'environnement](https://flask.palletsprojects.com/en/1.1.x/installation/#virtual-environments))
* pour gérer l'aléatoire:
  * random : https://docs.python.org/3/library/random.html#random.random
  * hashlib : https://docs.python.org/3/library/hashlib.html
* pour gérer le temps, time : https://docs.python.org/3/library/time.html#time.time
* pour l'écriture de fichiers, os : https://docs.python.org/3/library/os.path.html#os.path.exists

## Crédits image
https://commons.wikimedia.org/wiki/File:SARS-CoV-2_without_background.png

## Mise en place de l'environnement
voir sujet ou la [documentation flask](https://flask.palletsprojects.com/en/1.1.x/installation/) pour les différences de système d'exploitation…
1. Créer un fork pour votre groupe…
2. Cloner votre projet en local
3. Installer virtualenv (si pas encore fait)
  * mac, linux : ``pip install virtualenv``
    * (essayer ``pip3`` au lieu de ``pip`` si ça ne marche pas)
    * sous Mac si pip est vraiment inutilisable ça peut vouloir dire suivre un [tuto](https://docs.python-guide.org/starting/install3/osx/)
  * windows : **dans anaconda prompt** (si vous utilisez anaconda) ``pip install virtualenv``
    * si vous n'utilisez pas anaconda ou si ça ne marche pas vous pouvez essayer dans le prompt windows après avoir installé python via le Microsoft store
  * si rien ne marche vous pouvez tenter également :
    * ``pip3 install virtualenv``
    * ``py -m pip install virtualenv``
    * ``python -m pip install virtualenv``
    * ``python3 -m pip install virtualenv``
4. S'assurer que l'invite de commande (terminal, anaconda prompt, etc.) est bien dans le dossier du projet (chemin à gauche de l'écran)
5. Créer un environnement avec la commande  
``python -m venv venv-TPSID``
  * “``venv-TPSID``” est le nom de votre environnement à réutiliser après
  * si ça ne marche pas vous pouvez essayer avec ``py -m venv venv-TPSID`` ou ``python3 -m venv venv-TPSID``
6. Activer l'environnement  
``source venv-TPSID/bin/activate`` (Linux/Mac)  
``venv-TPSID\Scripts\activate`` (Windows)
6. [Installer flask](https://flask.palletsprojects.com/en/1.1.x/installation/#install-flask)  
Maintenant vous savez utiliser pip (cf. point 3, ci-dessus) → ``pip install flask``
7. Commencer à travailler (par exemple en exécutant ``python app.py``)

## À chaque nouvelle session de travail
1. venv: lancer l'environnement virtuel (point 6. ci-dessus)
2. git: « __puller__ » le code depuis le dépôt distant
3. travailler
4. git: « __commiter__ » vos modifications
5. git: « __pusher__ » le code modifier vers le dépôt distant
6. venv: ``deactivate`` l'environnement virtuel

**NB** : vous pouvez répéter les actions 2 à 5 plusieurs fois dans la même session.
